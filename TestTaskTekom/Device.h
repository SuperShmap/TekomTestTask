#pragma once

//Abstract base class for devices. It isn't hold ID because in
//child classes ID has different types. Also this class don't hold 
//data, because child classes SHOULD have different data.
//For now data in classes is just dummy.
class Device
{
public:

	virtual bool RecieveRequest(unsigned short ID) = 0;
	virtual bool SendAnswer() = 0;

	virtual ~Device();
};

