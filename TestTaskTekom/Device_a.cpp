#include "stdafx.h"
#include "Device_a.h"


Device_a::Device_a() : mData("Data succesfully returned")
{
}

bool Device_a::RecieveRequest(unsigned short ID)
{
	mID = static_cast<char>(ID);
	//Checking error that request has been sent to wrong device.
	if (mID != ID)
	{
		printf("Error handle request ID: %d. Device can't handle recieved ID. \n", ID);
		mID = NULL;
		return false;
	}
	else
		return true;
}

bool Device_a::SendAnswer()
{ 
	printf("%d %s \n", mID, mData.c_str());
	return true;
}