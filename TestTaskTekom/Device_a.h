#pragma once
#include "Device.h"
class Device_a : public Device
{
public:
	Device_a();

	virtual bool RecieveRequest(unsigned short ID);
	virtual bool SendAnswer();

private:

	uint8_t mID;
	const std::string mData;

};

