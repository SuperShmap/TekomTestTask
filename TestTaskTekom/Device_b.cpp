#include "stdafx.h"
#include "Device_b.h"


Device_b::Device_b() : mData("Data succesfully returned")
{
}

bool Device_b::RecieveRequest(unsigned short ID)
{
	//We dont need to check ID here because ID cant be more than 2 bytes
	mID = ID;
	return true;
}

bool Device_b::SendAnswer()
{
	char flag(0x23), beginTranslation(0x01), endTranslation(0x02);
	printf("%#x %#02x %d %s %#02x \n", flag, beginTranslation, mID, mData.c_str(), endTranslation);
	return true;
}