#pragma once
#include "Device.h"

class Device_b : public Device
{
public:

	Device_b();

	virtual bool RecieveRequest(unsigned short ID);
	virtual bool SendAnswer();

private:

	//This device recieving 2-byte ID 
	uint16_t mID;
	const std::string mData;

};


