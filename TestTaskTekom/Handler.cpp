#include "stdafx.h"
#include "Handler.h"


Handler::Handler()
{
	mDevices[0] = new Device_a;
	mDevices[2] = new Device_a;

	mDevices[1] = new Device_b;
	mDevices[3] = new Device_b;
}


bool Handler::SetRequest(unsigned char deviceNumber)
{
	if (deviceNumber > 4 || deviceNumber <= 0)
	{
		printf("Wrong device number \n");
		return false;
	}

	//First of all, we need to assign ID to request
	Request request;
	//We should to make sure that ID is unique, so we just raices the number
	//of buffer elements.
	request.ID = mBuffer.size();
	request.deviceNumber = deviceNumber - 1;
	mBuffer.push_back(request);
	
	if (mBuffer.size() >= mMaxBufferSize)
	{
		printf("Request buffer complete. Sending requests to devices \n");
		SendRequests();

		mBuffer.clear();
	}

	return true;
}

void Handler::SendRequests()
{
	for (int i = 0; i < mBuffer.size(); i++)
	{
		if (mBuffer[i].deviceNumber == 0 || mBuffer[i].deviceNumber == 2)
			RequestDeviceA(mBuffer[i]);
		else
			RequestDeviceB(mBuffer[i]);
	}
}

void Handler::RequestDeviceA(Request request)
{
	if (mDevices[request.deviceNumber]->RecieveRequest(request.ID))
	{
		printf("%d \n", request.ID);
		mDevices[request.deviceNumber]->SendAnswer();
	}
	else
		mErrorIDs.push_back(request.ID);
}

void Handler::RequestDeviceB(Request request)
{
	//If with device a-type request contain onli ID, b-type also 
	//has an additional fields
	char flag(0x23), beginTranslation(0x01), endTranslation(0x02);

	if (mDevices[request.deviceNumber]->RecieveRequest(request.ID))
	{
		printf("%#x %#x %d %#x \n", flag, beginTranslation, request.ID, endTranslation);
		mDevices[request.deviceNumber]->SendAnswer();
	}
	else
		mErrorIDs.push_back(request.ID);
}

Handler::~Handler()
{
	for (int i = 0; i < 4; i++)
		delete mDevices[i];
}