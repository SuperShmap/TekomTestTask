#pragma once
#include "Device.h"
#include "Device_a.h"
#include "Device_b.h"

class Handler
{
private:
	//Request buffer
	static const int mMaxBufferSize = 15;
	struct Request
	{
		int deviceNumber;
		unsigned short ID;
	};
	std::vector<Request> mBuffer;

public:
	Handler();
	~Handler();

	bool SetRequest(unsigned char deviceNumber);

private:
	void SendRequests();

	void RequestDeviceA(Request request);
	void RequestDeviceB(Request request);

	//Massive of our devices
	static const int mDevicesCount = 4;
	Device* mDevices[mDevicesCount];

	//Error log
	std::vector<unsigned short> mErrorIDs;
};